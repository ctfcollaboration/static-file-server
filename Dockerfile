FROM node:8-alpine

LABEL static-file-server="1.0.1"

USER node

RUN mkdir -p /home/node/app
WORKDIR /home/node/app

RUN mkdir static

COPY package.json .
RUN npm install --production

COPY . ./

EXPOSE 8080

ENTRYPOINT ["npm", "start"]
