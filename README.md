# Static File Server

This is a very simple HTTP File server that is implemented in Node Js.
It uses Express JS to serve static files from a folder that exists on the host machine.

## Default Endpoints

There is an endpoint `health-check` which simply returns a `200 OK` when accessed. This is just to provide a simple sanity check to make sure the app is up and running

## Running

You can run this by simply running `npm start `from the project root directory. You can add static content by creating a folder named `static` in the project root and adding your files to it.

By default the application will be running on port `8080`

## Running in Docker

A Dockerfile has been included and you can build a new image by running the following

```docker image build -t static-file-server:latest .```

An example of how to run a container is shown below

```
docker container run -d \
--name static-server \
-p 8080:8080 \
-v "$(pwd)"/static:/home/node/app/static \
static-file-server:latest
```

The app is exposed on port 8080 and this is what we also to bind with from the host in the example

Additionally, we have created a bind mount so that we can populate the static folder from the host. Any changes will immediate.

## Pushing to AWS ECR

The ECR Repository gives you instructions on how to login, build and push your docker image. The steps are broadly outlined below.

```
# Docker Login
$(aws ecr get-login --profile <profile> --no-include-email --region eu-west-2)

# Build Image
docker build -t static-file-server .

# You need to tag the build in a certain way
docker tag static-file-server:latest <account>.dkr.ecr.eu-west-2.amazonaws.com/static-file-server:latest
docker tag static-file-server:latest <account>.dkr.ecr.eu-west-2.amazonaws.com/static-file-server:<version>

# Push the image to ECR
docker push <account>.dkr.ecr.eu-west-2.amazonaws.com/static-file-server:latest
docker push <account>.dkr.ecr.eu-west-2.amazonaws.com/static-file-server:<version>
```
