const express = require('express');
const app = express();

const PORT = process.env.PORT || 8080

app.use(require('helmet')());
app.use(express.static('static'));

app.get('/health-check', (req, res) => res.sendStatus(200));

// Start the Server
app.listen(PORT, () => {
  console.log(`HTTP server started on port ${PORT}`)
});
